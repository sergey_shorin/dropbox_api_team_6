# Dropbox_api_team_6

## Simple requests project

This project is used to show how to write custom CLI tools. This CLI tool allows downloading files from the Internet and printing text responses from web-pages.

## Installation and usage
You need at least `Python 3.8` and `pip` installed to run this CLI tool.
1. Install it using `pip`
    ```shell
    pip3 install dropbox_api_team_6
    ```
1. Call `--help` to ensure that it is successfully installed
    ```shell
    simple_requests --help
    ```
1. Download a file using installed `simple_requests`
    ```shell
    simple_requests download-file https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg
    ```
    The file will be saved into `./downloaded_files` directory.
1. Print a web-page using `simple_requests`
    ```shell
    simple_requests print-response https://python.org
    ```
   HTML content of https://python.org will be printed.

